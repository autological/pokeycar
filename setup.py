from setuptools import find_packages
from setuptools import setup

setup(
    name='pokeycar',
    version='2.5.1',
    install_requires=[
                'donkeycar==2.5.1',
                'google-cloud-storage',
                'google-resumable-media'
                ],
    packages=find_packages(),
    include_package_data=True
)